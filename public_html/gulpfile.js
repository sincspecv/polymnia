/*----------------------------------------------------------------------
# Abstrakt Gulpfile
	Use to compile SASS, browser sync, and optimize files for production
----------------------------------------------------------------------*/

/*
	Set the specifics for your project
*/

// Change domain and child theme
const domain 		= 'http://polymnia.dev';

// Add array of URLs used to uncss and generate critical CSS
// Include each unique template (don't forget blog/posts)
const pages = [
  domain+'/'
];

/*--------------------------------------------------------------
# Install Gulp and plugins
	A good resource -- https://css-tricks.com/gulp-for-beginners/
----------------------------------------------------------------

// Install Gulp in directory (assumes Gulp is already installed globally)

In terminal:

	$ cd path/to/directory
	$ npm install

----------------------------------------------------------------
# Tasks
----------------------------------------------------------------

gulp watch:
	- compile SASS and monitor changes

gulp prepare:
	- concatenate and minify js files
	- remove unneeded css rules
	- prefix css (two versions back)
	- extract and inline critical CSS
	- minify site.css

gulp optimize:
	- minify and inline critical css

---------------------------------------------------------------*/

// Basic workflow plugins
const gulp        	= require('gulp');
const rename      	= require('gulp-rename');
const sass        	= require('gulp-sass');
const browserSync 	= require('browser-sync').create();
const build 		= require('gulp-build');
const runSequence 	= require('run-sequence');

//CSS Processing (PostCSS)
const postcss		= require('gulp-postcss');
const sourcemaps 	= require('gulp-sourcemaps');
const autoprefixer 	= require('autoprefixer');
const cssnext 		= require('cssnext');
const mqpacker 		= require('css-mqpacker');
const rucksack  	= require('rucksack-css');

// Performance workflow plugins
const uncss 		= require('gulp-uncss');
const prefix   	 	= require('gulp-autoprefixer');
const uglify 		= require('gulp-uglify');
const concat 		= require('gulp-concat');
const concatUtil 	= require('gulp-concat-util');
const penthouse 	= require('penthouse');
const cleanCSS 		= require('gulp-clean-css');
const concatCss 	= require('gulp-concat-css');

// Error handling
const plumber 		= require('gulp-plumber');
const notify        = require('gulp-notify');
const onError       = (err) => {
  notify.onError({
    title:    "Gulp error in " + err.plugin,
    message:  err.toString()
  })(err);
  console.log(err.toString());
  // this.emit('end');
};

// Paths
const parent 		= `${__dirname}/wp-content/themes/polymnia`;

/*--------------------------------------------------------------
# Development tasks
--------------------------------------------------------------*/

// SASS
gulp.task('sass', function() {
  return gulp.src(`${parent}/sass/*.scss`)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass())
    .pipe(postcss([ rucksack, cssnext, autoprefixer({browsers: ['last 4 versions']}) ]))
    .pipe(rename('site.css'))
    .pipe(gulp.dest(`${parent}/styles`))
    .pipe(browserSync.stream());
});

//PostCSS
// gulp.task('autoprefix', function() {
// 	return gulp.src(`${child}/styles/site.css`)
// 		.pipe(plumber({
// 			errorHandler: onError
// 		}))
// 		.pipe(sourcemaps.init())
// 		.pipe(postcss([ autoprefixer({browsers: ['last 4 versions']}), cssnext ]))
// 		.pipe(sourcemaps.write('.'))
// 		.pipe(gulp.dest(`${child}/styles`))
//         .pipe(browserSync.reload({
//             stream: true
//         }));
// });

// BrowserSync
gulp.task('browserSync', function() {
  browserSync.init({
    proxy: domain
  });
});

// Watch
gulp.task('watch', ['browserSync', 'sass'], function(){
  gulp.watch(parent+'/sass/*.scss', ['sass']);
  gulp.watch(parent+'/sass/**/*.scss', ['sass']);
  // gulp.watch(child+'/sass/*.scss', ['sass']);
  gulp.watch(parent+'/**/*.php', browserSync.reload);
  // gulp.watch(child+'/**/*.php', browserSync.reload);
  gulp.watch(parent+'/js/*.js', browserSync.reload);
  // gulp.watch(child+'/js/*.js', browserSync.reload);
});

/*--------------------------------------------------------------
# Production tasks
--------------------------------------------------------------*/

// Concat css
gulp.task('concatCSS', function () {
  return gulp.src([
    child+'/styles/site.css',
    './wp-content/plugins/gravityforms/css/browsers.css',
    './wp-content/plugins/gravityforms/css/readyclass.css',
    './wp-content/plugins/gravityforms/css/formsmain.css',
    './wp-content/plugins/gravityforms/css/formreset.css',
  ])
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest(child+'/styles'));
});

// Remove unneeded CSS
gulp.task('uncss', function() {
  return gulp.src(child+'/styles/bundle.css')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(uncss({
      html: pages,
      ignore: [/\.open/, /\.amg-submenu-open/, /\.logged-in/]
    }))
    .pipe(rename('pure.css'))
    .pipe(gulp.dest(child+'/styles'))
});

// Prefix CSS
gulp.task('prefix', function(){
  return gulp.src(child+'/styles/pure.css')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(prefix('last 4 versions'))
    .pipe(rename('prefixed.css'))
    .pipe(gulp.dest(child+'/styles'))
});

// Minify JS
gulp.task('minifyJS', function() {
  return gulp.src([
    child+'/js/*.js',
    parent+'/js/*.js',
    '!'+parent+'/js/abstrakt-admin-scripts.js',
    '!'+parent+'/js/customizer.js'
  ])
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(child+'/js'))
    .pipe(rename('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(child+'/js'));
});

// Extract Critical CSS
gulp.task('critical', function() {
  pages.map(function(page) {
    penthouse({
      url: page,
      css: child+'/styles/prefixed.css',
      width: 1280,
      height: 1280
    }, function(err, data) {
      require('fs').writeFile(child+'/styles/critical.css', data);
    });
  });
});

// Minify CSS
gulp.task('cleanCSS', () => {
  return gulp.src(child+'/styles/prefixed.css')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cleanCSS(
      {
        debug: true
      },
      function(details) {
        console.log(details.name + ': ' + details.stats.originalSize);
        console.log(details.name + ': ' + details.stats.minifiedSize);
      }
    ))
    .pipe(rename('site.min.css'))
    .pipe(gulp.dest(child+'/styles'))
});

// Run production tasks
gulp.task('prepare', function (callback) {
  runSequence('sass', 'concatCSS', 'uncss', 'prefix','critical','cleanCSS', 'minifyJS', callback);
});

// Minify Critical CSS
gulp.task('optimize', function() {
  return gulp.src(child+'/styles/critical.css')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(cleanCSS())
    .pipe(concatUtil.header('<style>'))
    .pipe(concatUtil.footer('</style>'))
    .pipe(rename({
      basename: 'critical.css',
      extname: '.php'
    }))
    .pipe(gulp.dest(child+'/styles/'));
});
