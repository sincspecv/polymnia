<?php
/**
 * POLYMNIA instagram integration
 *
 * @package POLYMNIA
 * @since 1.0.0
 */

if( ! class_exists(  'Polymnia_Instagram' ) ) {
	class Polymnia_Instagram {

		function __construct() {
			add_action('polymnia_admin_page', array( $this, 'polymnia_instagram_admin_fields' ) );
		}

		public function polymnia_instagram_admin_fields() {
			echo 'Instagram';
		}
	}
	new Polymnia_Instagram();
}

