<?php
/**
 * Register post type for show schedule
 *
 * @since 1.0.0
 */


if( ! function_exists( 'polymnia_show_schedule' ) ) :
    function polymnia_show_schedule() {

        $labels = array(
            'name'                  => _x( 'Show List', 'Post Type General Name', 'polymnia' ),
            'singular_name'         => _x( 'Show', 'Post Type Singular Name', 'polymnia' ),
            'menu_name'             => __( 'Show List', 'polymnia' ),
            'name_admin_bar'        => __( 'Show List', 'polymnia' ),
            'archives'              => __( 'Show Archives', 'polymnia' ),
            'attributes'            => __( 'Show Attributes', 'polymnia' ),
            'parent_item_colon'     => __( '', 'polymnia' ),
            'all_items'             => __( 'All Shows', 'polymnia' ),
            'add_new_item'          => __( 'Add New Show', 'polymnia' ),
            'add_new'               => __( 'Add Show', 'polymnia' ),
            'new_item'              => __( 'New Show', 'polymnia' ),
            'edit_item'             => __( 'Edit Show Listing', 'polymnia' ),
            'update_item'           => __( 'Update Show Listing', 'polymnia' ),
            'view_item'             => __( 'View Show Listing', 'polymnia' ),
            'view_items'            => __( 'View Shows', 'polymnia' ),
            'search_items'          => __( 'Search Shows', 'polymnia' ),
            'not_found'             => __( 'No Shows Found', 'polymnia' ),
            'not_found_in_trash'    => __( 'No Shows Found In Trash', 'polymnia' ),
            'featured_image'        => __( 'Featured Image', 'polymnia' ),
            'set_featured_image'    => __( 'Set featured image', 'polymnia' ),
            'remove_featured_image' => __( 'Remove featured image', 'polymnia' ),
            'use_featured_image'    => __( 'Use as featured image', 'polymnia' ),
            'insert_into_item'      => __( 'Insert Into Show Listing', 'polymnia' ),
            'uploaded_to_this_item' => __( 'Uploaded To Show Listing', 'polymnia' ),
            'items_list'            => __( 'Show List', 'polymnia' ),
            'items_list_navigation' => __( 'Show list navigation', 'polymnia' ),
            'filter_items_list'     => __( 'Filter Show List', 'polymnia' ),
        );
        $rewrite = array(
            'slug'                  => 'show_list',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args = array(
            'label'                 => __( 'Show', 'polymnia' ),
            'description'           => __( 'List of shows', 'polymnia' ),
            'labels'                => $labels,
            'supports'              => array( 'revisions' ),
            'taxonomies'            => array( 'date', 'venue', 'city', 'showtime', 'guests', 'ticket_link' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => false,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-tickets-alt',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
            'show_in_rest'          => true,
            'rest_base'             => 'shows',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
        );
        register_post_type( 'polymnia_shows', $args );

    }
    add_action( 'init', 'polymnia_show_schedule', 0 );
endif;


/**
 * Filter the settings for the show schedule editor
 * @since 1.0.0
 * @package Polymnia
 * @param 	string 		$editor_id 		HTML ID attribute value for the textarea and TinyMCE. Can only be /[a-z]+/.
 * @param 	array  		$settings  		See _WP_Editors::editor().
 * @return 	array 		$settings
 *
 **/
if( ! function_exists( 'polymnia_show_schedule_wp_editor_settings' ) ) {
	function polymnia_show_schedule_wp_editor_settings( $settings, $editor_id ) {
		if ( $editor_id === 'content' && get_current_screen()->post_type === 'show_list' ) {
			$settings['tinymce']   = false;
			$settings['quicktags'] = false;
			$settings['media_buttons'] = false;
		}

		return $settings;
	}
	add_filter( 'wp_editor_settings', 'polymnia_show_schedule_wp_editor_settings(', 10, 3 );
}


/**
 * Show Schedule Post Meta
 *
 * @since 1.0.0
 *
 */

if ( ! function_exists( 'polymnia_show_schedule_meta_boxes' ) ) :
	function polymnia_show_schedule_meta_boxes() {
		add_meta_box( 'polymnia-show-info', __( 'Show Info', 'polymnia' ), 'polymnia_show_info_callback', 'polymnia_shows', 'advanced', 'high' );
	}
	add_action( 'add_meta_boxes', 'polymnia_show_schedule_meta_boxes' );
endif;

/**
 * Metabox for show info
 *
 * @since 1.0.0
 *
 */

if( ! function_exists('polymnia_show_info_callback') ) {
    function polymnia_show_info_callback() {
	    global $post;

	    $show_date = get_post_meta( $post->ID, '_polymnia-show-date', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_date_nonce' );

	    $show_venue = get_post_meta( $post->ID, '_polymnia-show-venue', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_venue_nonce' );

	    $show_city = get_post_meta( $post->ID, '_polymnia-show-city', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_city_nonce' );

	    $show_time = get_post_meta( $post->ID, '_polymnia-show-showtime', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_showtime_nonce' );

	    $show_guests = get_post_meta( $post->ID, '_polymnia-show-guests', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_guests_nonce' );

	    $show_tickets_link = get_post_meta( $post->ID, '_polymnia-show-tickets-link', true );
	    wp_nonce_field( 'polymnia_show_schedule_nonce_action', 'polymnia_show_tickets_link_nonce' );

        ?>
        <script type="text/javascript">
          /*!
		  * Datepicker v0.6.3
		  * https://github.com/fengyuanchen/datepicker
		  *
		  * Copyright (c) 2014-2017 Fengyuan Chen
		  * Released under the MIT license
		  *
		  * Date: 2017-09-29T14:28:06.767Z
		  */
          var $ = jQuery;
          !function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],t):t(e.jQuery)}(this,function(e){"use strict";function t(e){return k.call(e).slice(8,-1).toLowerCase()}function i(e){return"string"==typeof e}function a(e){return"number"==typeof e&&!w(e)}function s(e){return void 0===e}function n(e){return"date"===t(e)}function r(e,t){for(var i=arguments.length,a=Array(i>2?i-2:0),s=2;s<i;s++)a[s-2]=arguments[s];return function(){for(var i=arguments.length,s=Array(i),n=0;n<i;n++)s[n]=arguments[n];return e.apply(t,a.concat(s))}}function h(e){return'[data-view="'+e+'"]'}function o(e){return e%4==0&&e%100!=0||e%400==0}function l(e,t){return[31,o(e)?29:28,31,30,31,30,31,31,30,31,30,31][t]}function d(e,t,i){return Math.min(i,l(e,t))}function u(t){var i=String(t).toLowerCase(),a=i.match(D);if(!a||0===a.length)throw new Error("Invalid date format.");return t={source:i,parts:a},e.each(a,function(e,i){switch(i){case"dd":case"d":t.hasDay=!0;break;case"mm":case"m":t.hasMonth=!0;break;case"yyyy":case"yy":t.hasYear=!0}}),t}function c(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}e=e&&e.hasOwnProperty("default")?e.default:e;var p={autoShow:!1,autoHide:!1,autoPick:!1,inline:!1,container:null,trigger:null,language:"",format:"mm/dd/yyyy",date:null,startDate:null,endDate:null,startView:0,weekStart:0,yearFirst:!1,yearSuffix:"",days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],daysMin:["Su","Mo","Tu","We","Th","Fr","Sa"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],itemTag:"li",mutedClass:"muted",pickedClass:"picked",disabledClass:"disabled",highlightedClass:"highlighted",template:'<div class="datepicker-container"><div class="datepicker-panel" data-view="years picker"><ul><li data-view="years prev">&lsaquo;</li><li data-view="years current"></li><li data-view="years next">&rsaquo;</li></ul><ul data-view="years"></ul></div><div class="datepicker-panel" data-view="months picker"><ul><li data-view="year prev">&lsaquo;</li><li data-view="year current"></li><li data-view="year next">&rsaquo;</li></ul><ul data-view="months"></ul></div><div class="datepicker-panel" data-view="days picker"><ul><li data-view="month prev">&lsaquo;</li><li data-view="month current"></li><li data-view="month next">&rsaquo;</li></ul><ul data-view="week"></ul><ul data-view="days"></ul></div></div>',offset:10,zIndex:1e3,filter:null,show:null,hide:null,pick:null},f="datepicker",g="click.datepicker",y="datepicker-hide",v={},m={DAYS:0,MONTHS:1,YEARS:2},k=Object.prototype.toString,w=Number.isNaN||window.isNaN,D=/(y|m|d)+/g,b=window.document,C=e(window),$=e(b),x=/\d+/g,M={show:function(){this.built||this.build(),this.shown||this.trigger("show.datepicker").isDefaultPrevented()||(this.shown=!0,this.$picker.removeClass(y).on(g,e.proxy(this.click,this)),this.showView(this.options.startView),this.inline||(C.on("resize.datepicker",this.onResize=r(this.place,this)),$.on(g,this.onGlobalClick=r(this.globalClick,this)),$.on("keyup.datepicker",this.onGlobalKeyup=r(this.globalKeyup,this)),this.place()))},hide:function(){this.shown&&(this.trigger("hide.datepicker").isDefaultPrevented()||(this.shown=!1,this.$picker.addClass(y).off(g,this.click),this.inline||(C.off("resize.datepicker",this.onResize),$.off(g,this.onGlobalClick),$.off("keyup.datepicker",this.onGlobalKeyup))))},toggle:function(){this.shown?this.hide():this.show()},update:function(){var e=this.getValue();e!==this.oldValue&&(this.setDate(e,!0),this.oldValue=e)},pick:function(e){var t=this.$element,i=this.date;this.trigger("pick.datepicker",{view:e||"",date:i}).isDefaultPrevented()||(i=this.formatDate(this.date),this.setValue(i),this.isInput&&(t.trigger("input"),t.trigger("change")))},reset:function(){this.setDate(this.initialDate,!0),this.setValue(this.initialValue),this.shown&&this.showView(this.options.startView)},getMonthName:function(t,i){var n=this.options,r=n.monthsShort,h=n.months;return e.isNumeric(t)?t=Number(t):s(i)&&(i=t),!0===i&&(h=r),h[a(t)?t:this.date.getMonth()]},getDayName:function(t,i,n){var r=this.options,h=r.days;return e.isNumeric(t)?t=Number(t):(s(n)&&(n=i),s(i)&&(i=t)),n?h=r.daysMin:i&&(h=r.daysShort),h[a(t)?t:this.date.getDay()]},getDate:function(e){var t=this.date;return e?this.formatDate(t):new Date(t)},setDate:function(t,a){var s=this.options.filter;if(n(t)||i(t)){if(t=this.parseDate(t),e.isFunction(s)&&!1===s.call(this.$element,t))return;this.date=t,this.viewDate=new Date(t),a||this.pick(),this.built&&this.render()}},setStartDate:function(e){(n(e)||i(e))&&(this.startDate=this.parseDate(e),this.built&&this.render())},setEndDate:function(e){(n(e)||i(e))&&(this.endDate=this.parseDate(e),this.built&&this.render())},parseDate:function(t){var a=this.format,s=[];if(n(t))return new Date(t.getFullYear(),t.getMonth(),t.getDate());i(t)&&(s=t.match(x)||[]),t=new Date;var r=a.parts.length,h=t.getFullYear(),o=t.getDate(),l=t.getMonth();return s.length===r&&e.each(s,function(e,t){var i=parseInt(t,10)||1;switch(a.parts[e]){case"dd":case"d":o=i;break;case"mm":case"m":l=i-1;break;case"yy":h=2e3+i;break;case"yyyy":h=i}}),new Date(h,l,o)},formatDate:function(t){var i=this.format,a="";if(n(t)){var s=t.getFullYear(),r={d:t.getDate(),m:t.getMonth()+1,yy:s.toString().substring(2),yyyy:s};r.dd=(r.d<10?"0":"")+r.d,r.mm=(r.m<10?"0":"")+r.m,a=i.source,e.each(i.parts,function(e,t){a=a.replace(t,r[t])})}return a},destroy:function(){this.unbind(),this.unbuild(),this.$element.removeData(f)}},S={click:function(t){var i=e(t.target),a=this.options,s=this.viewDate,n=this.format;if(t.stopPropagation(),t.preventDefault(),!i.hasClass("disabled")){var r=i.data("view"),h=s.getFullYear(),o=s.getMonth(),l=s.getDate();switch(r){case"years prev":case"years next":h="years prev"===r?h-10:h+10,this.viewDate=new Date(h,o,d(h,o,l)),this.renderYears();break;case"year prev":case"year next":h="year prev"===r?h-1:h+1,this.viewDate=new Date(h,o,d(h,o,l)),this.renderMonths();break;case"year current":n.hasYear&&this.showView(m.YEARS);break;case"year picked":n.hasMonth?this.showView(m.MONTHS):(i.addClass(a.pickedClass).siblings().removeClass(a.pickedClass),this.hideView()),this.pick("year");break;case"year":h=parseInt(i.text(),10),this.date=new Date(h,o,d(h,o,l)),n.hasMonth?(this.viewDate=new Date(this.date),this.showView(m.MONTHS)):(i.addClass(a.pickedClass).siblings().removeClass(a.pickedClass),this.hideView()),this.pick("year");break;case"month prev":case"month next":(o="month prev"===r?o-1:o+1)<0?(h-=1,o+=12):o>11&&(h+=1,o-=12),this.viewDate=new Date(h,o,d(h,o,l)),this.renderDays();break;case"month current":n.hasMonth&&this.showView(m.MONTHS);break;case"month picked":n.hasDay?this.showView(m.DAYS):(i.addClass(a.pickedClass).siblings().removeClass(a.pickedClass),this.hideView()),this.pick("month");break;case"month":o=e.inArray(i.text(),a.monthsShort),this.date=new Date(h,o,d(h,o,l)),n.hasDay?(this.viewDate=new Date(h,o,d(h,o,l)),this.showView(m.DAYS)):(i.addClass(a.pickedClass).siblings().removeClass(a.pickedClass),this.hideView()),this.pick("month");break;case"day prev":case"day next":case"day":"day prev"===r?o-=1:"day next"===r&&(o+=1),l=parseInt(i.text(),10),this.date=new Date(h,o,l),this.viewDate=new Date(h,o,l),this.renderDays(),"day"===r&&this.hideView(),this.pick("day");break;case"day picked":this.hideView(),this.pick("day")}}},globalClick:function(e){for(var t=e.target,i=this.element,a=this.$trigger[0],s=!0;t!==document;){if(t===a||t===i){s=!1;break}t=t.parentNode}s&&this.hide()},keyup:function(){this.update()},globalKeyup:function(e){var t=e.target,i=e.key,a=e.keyCode;this.isInput&&t!==this.element&&this.shown&&("Tab"===i||9===a)&&this.hide()}},F={render:function(){this.renderYears(),this.renderMonths(),this.renderDays()},renderWeek:function(){var t=this,i=[],a=this.options,s=a.weekStart,n=a.daysMin;s=parseInt(s,10)%7,n=n.slice(s).concat(n.slice(0,s)),e.each(n,function(e,a){i.push(t.createItem({text:a}))}),this.$week.html(i.join(""))},renderYears:function(){var e=this.options,t=this.startDate,i=this.endDate,a=e.disabledClass,s=e.filter,n=e.yearSuffix,r=this.viewDate.getFullYear(),h=(new Date).getFullYear(),o=this.date.getFullYear(),l=[],d=!1,u=!1,c=void 0;for(c=-5;c<=6;c+=1){var p=new Date(r+c,1,1),f=!1;t&&(f=p.getFullYear()<t.getFullYear(),-5===c&&(d=f)),!f&&i&&(f=p.getFullYear()>i.getFullYear(),6===c&&(u=f)),!f&&s&&(f=!1===s.call(this.$element,p));var g=r+c===o,y=g?"year picked":"year";l.push(this.createItem({picked:g,disabled:f,muted:-5===c||6===c,text:r+c,view:f?"year disabled":y,highlighted:p.getFullYear()===h}))}this.$yearsPrev.toggleClass(a,d),this.$yearsNext.toggleClass(a,u),this.$yearsCurrent.toggleClass(a,!0).html(r+-5+n+" - "+(r+6)+n),this.$years.html(l.join(""))},renderMonths:function(){var t=this.options,i=this.startDate,a=this.endDate,s=this.viewDate,n=t.disabledClass||"",r=t.monthsShort,h=e.isFunction(t.filter)&&t.filter,o=s.getFullYear(),l=new Date,d=l.getFullYear(),u=l.getMonth(),c=this.date.getFullYear(),p=this.date.getMonth(),f=[],g=!1,y=!1,v=void 0;for(v=0;v<=11;v+=1){var m=new Date(o,v,1),k=!1;i&&(k=(g=m.getFullYear()===i.getFullYear())&&m.getMonth()<i.getMonth()),!k&&a&&(k=(y=m.getFullYear()===a.getFullYear())&&m.getMonth()>a.getMonth()),!k&&h&&(k=!1===h.call(this.$element,m));var w=o===c&&v===p,D=w?"month picked":"month";f.push(this.createItem({disabled:k,picked:w,highlighted:o===d&&m.getMonth()===u,index:v,text:r[v],view:k?"month disabled":D}))}this.$yearPrev.toggleClass(n,g),this.$yearNext.toggleClass(n,y),this.$yearCurrent.toggleClass(n,g&&y).html(o+t.yearSuffix||""),this.$months.html(f.join(""))},renderDays:function(){var e=this.$element,t=this.options,i=this.startDate,a=this.endDate,s=this.viewDate,n=this.date,r=t.disabledClass,h=t.filter,o=t.monthsShort,d=t.weekStart,u=t.yearSuffix,c=s.getFullYear(),p=s.getMonth(),f=new Date,g=f.getFullYear(),y=f.getMonth(),v=f.getDate(),m=n.getFullYear(),k=n.getMonth(),w=n.getDate(),D=void 0,b=void 0,C=void 0,$=[],x=c,M=p,S=!1;0===p?(x-=1,M=11):M-=1,D=l(x,M);var F=new Date(c,p,1);for((C=F.getDay()-parseInt(d,10)%7)<=0&&(C+=7),i&&(S=F.getTime()<=i.getTime()),b=D-(C-1);b<=D;b+=1){var Y=new Date(x,M,b),T=!1;i&&(T=Y.getTime()<i.getTime()),!T&&h&&(T=!1===h.call(e,Y)),$.push(this.createItem({disabled:T,highlighted:x===g&&M===y&&Y.getDate()===v,muted:!0,picked:x===m&&M===k&&b===w,text:b,view:"day prev"}))}var V=[],I=c,N=p,P=!1;11===p?(I+=1,N=0):N+=1,D=l(c,p),C=42-($.length+D);var A=new Date(c,p,D);for(a&&(P=A.getTime()>=a.getTime()),b=1;b<=C;b+=1){var j=new Date(I,N,b),O=I===m&&N===k&&b===w,H=!1;a&&(H=j.getTime()>a.getTime()),!H&&h&&(H=!1===h.call(e,j)),V.push(this.createItem({disabled:H,picked:O,highlighted:I===g&&N===y&&j.getDate()===v,muted:!0,text:b,view:"day next"}))}var q=[];for(b=1;b<=D;b+=1){var E=new Date(c,p,b),z=!1;i&&(z=E.getTime()<i.getTime()),!z&&a&&(z=E.getTime()>a.getTime()),!z&&h&&(z=!1===h.call(e,E));var W=c===m&&p===k&&b===w,J=W?"day picked":"day";q.push(this.createItem({disabled:z,picked:W,highlighted:c===g&&p===y&&E.getDate()===v,text:b,view:z?"day disabled":J}))}this.$monthPrev.toggleClass(r,S),this.$monthNext.toggleClass(r,P),this.$monthCurrent.toggleClass(r,S&&P).html(t.yearFirst?c+u+" "+o[p]:o[p]+" "+c+u),this.$days.html($.join("")+q.join("")+V.join(""))}},Y=function(){function e(e,t){for(var i=0;i<t.length;i++){var a=t[i];a.enumerable=a.enumerable||!1,a.configurable=!0,"value"in a&&(a.writable=!0),Object.defineProperty(e,a.key,a)}}return function(t,i,a){return i&&e(t.prototype,i),a&&e(t,a),t}}(),T=window.document,V=e(T),I=["datepicker-top-left","datepicker-top-right","datepicker-bottom-left","datepicker-bottom-right"].join(" "),N=function(){function t(i){var a=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};c(this,t),this.$element=e(i),this.element=i,this.options=e.extend({},p,v[a.language],a),this.built=!1,this.shown=!1,this.isInput=!1,this.inline=!1,this.initialValue="",this.initialDate=null,this.startDate=null,this.endDate=null,this.init()}return Y(t,[{key:"init",value:function(){var t=this.$element,i=this.options,a=i.startDate,s=i.endDate,n=i.date;this.$trigger=e(i.trigger),this.isInput=t.is("input")||t.is("textarea"),this.inline=i.inline&&(i.container||!this.isInput),this.format=u(i.format);var r=this.getValue();this.initialValue=r,this.oldValue=r,n=this.parseDate(n||r),a&&(a=this.parseDate(a),n.getTime()<a.getTime()&&(n=new Date(a)),this.startDate=a),s&&(s=this.parseDate(s),a&&s.getTime()<a.getTime()&&(s=new Date(a)),n.getTime()>s.getTime()&&(n=new Date(s)),this.endDate=s),this.date=n,this.viewDate=new Date(n),this.initialDate=new Date(this.date),this.bind(),(i.autoShow||this.inline)&&this.show(),i.autoPick&&this.pick()}},{key:"build",value:function(){if(!this.built){this.built=!0;var t=this.$element,i=this.options,a=e(i.template);this.$picker=a,this.$week=a.find(h("week")),this.$yearsPicker=a.find(h("years picker")),this.$yearsPrev=a.find(h("years prev")),this.$yearsNext=a.find(h("years next")),this.$yearsCurrent=a.find(h("years current")),this.$years=a.find(h("years")),this.$monthsPicker=a.find(h("months picker")),this.$yearPrev=a.find(h("year prev")),this.$yearNext=a.find(h("year next")),this.$yearCurrent=a.find(h("year current")),this.$months=a.find(h("months")),this.$daysPicker=a.find(h("days picker")),this.$monthPrev=a.find(h("month prev")),this.$monthNext=a.find(h("month next")),this.$monthCurrent=a.find(h("month current")),this.$days=a.find(h("days")),this.inline?e(i.container||t).append(a.addClass("datepicker-inline")):(e(T.body).append(a.addClass("datepicker-dropdown")),a.addClass(y)),this.renderWeek()}}},{key:"unbuild",value:function(){this.built&&(this.built=!1,this.$picker.remove())}},{key:"bind",value:function(){var t=this.options,i=this.$element;e.isFunction(t.show)&&i.on("show.datepicker",t.show),e.isFunction(t.hide)&&i.on("hide.datepicker",t.hide),e.isFunction(t.pick)&&i.on("pick.datepicker",t.pick),this.isInput&&i.on("keyup.datepicker",e.proxy(this.keyup,this)),this.inline||(t.trigger?this.$trigger.on(g,e.proxy(this.toggle,this)):this.isInput?i.on("focus.datepicker",e.proxy(this.show,this)):i.on(g,e.proxy(this.show,this)))}},{key:"unbind",value:function(){var t=this.$element,i=this.options;e.isFunction(i.show)&&t.off("show.datepicker",i.show),e.isFunction(i.hide)&&t.off("hide.datepicker",i.hide),e.isFunction(i.pick)&&t.off("pick.datepicker",i.pick),this.isInput&&t.off("keyup.datepicker",this.keyup),this.inline||(i.trigger?this.$trigger.off(g,this.toggle):this.isInput?t.off("focus.datepicker",this.show):t.off(g,this.show))}},{key:"showView",value:function(e){var t=this.$yearsPicker,i=this.$monthsPicker,a=this.$daysPicker,s=this.format;if(s.hasYear||s.hasMonth||s.hasDay)switch(Number(e)){case m.YEARS:i.addClass(y),a.addClass(y),s.hasYear?(this.renderYears(),t.removeClass(y),this.place()):this.showView(m.DAYS);break;case m.MONTHS:t.addClass(y),a.addClass(y),s.hasMonth?(this.renderMonths(),i.removeClass(y),this.place()):this.showView(m.YEARS);break;default:t.addClass(y),i.addClass(y),s.hasDay?(this.renderDays(),a.removeClass(y),this.place()):this.showView(m.MONTHS)}}},{key:"hideView",value:function(){!this.inline&&this.options.autoHide&&this.hide()}},{key:"place",value:function(){if(!this.inline){var e=this.$element,t=this.options,i=this.$picker,a=V.outerWidth(),s=V.outerHeight(),n=e.outerWidth(),r=e.outerHeight(),h=i.width(),o=i.height(),l=e.offset(),d=l.left,u=l.top,c=parseFloat(t.offset),p="datepicker-top-left";w(c)&&(c=10),u>o&&u+r+o>s?(u-=o+c,p="datepicker-bottom-left"):u+=r+c,d+h>a&&(d+=n-h,p=p.replace("left","right")),i.removeClass(I).addClass(p).css({top:u,left:d,zIndex:parseInt(t.zIndex,10)})}}},{key:"trigger",value:function(t,i){var a=e.Event(t,i);return this.$element.trigger(a),a}},{key:"createItem",value:function(t){var i=this.options,a=i.itemTag,s={text:"",view:"",muted:!1,picked:!1,disabled:!1,highlighted:!1},n=[];return e.extend(s,t),s.muted&&n.push(i.mutedClass),s.highlighted&&n.push(i.highlightedClass),s.picked&&n.push(i.pickedClass),s.disabled&&n.push(i.disabledClass),"<"+a+' class="'+n.join(" ")+'" data-view="'+s.view+'">'+s.text+"</"+a+">"}},{key:"getValue",value:function(){var e=this.$element;return this.isInput?e.val():e.text()}},{key:"setValue",value:function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",t=this.$element;this.isInput?t.val(e):t.text(e)}}],[{key:"setDefaults",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};e.extend(p,v[t.language],t)}}]),t}();e.extend(N.prototype,F,S,M);var P=e.fn.datepicker;e.fn.datepicker=function(t){for(var i=arguments.length,a=Array(i>1?i-1:0),s=1;s<i;s++)a[s-1]=arguments[s];var n=void 0;return this.each(function(){var i=e(this),s=i.data("datepicker");if(!s){if(/destroy/.test(t))return;var r=e.extend({},i.data(),e.isPlainObject(t)&&t);s=new N(this,r),i.data("datepicker",s)}if("string"==typeof t){var h=s[t];e.isFunction(h)&&(n=h.apply(s,a))}}),void 0!==n?n:this},e.fn.datepicker.Constructor=N,e.fn.datepicker.languages=v,e.fn.datepicker.setDefaults=N.setDefaults,e.fn.datepicker.noConflict=function(){return e.fn.datepicker=P,this}});

          $('[data-toggle="datepicker"]').datepicker();
        </script>
        <style>
            #normal-sortables {
                display: none;
            }

            #post-body-content {
                display: none;
            }

            .inside {
                display: inline-block;
            }

            .polymnia-show-field {
                max-width: 300px;
                display: inline-block;
                float: left;
            }

            .polymnia-show-field label {
                display: block;
                clear: both;
            }

            /*!
             * Datepicker v0.6.3
             * https://github.com/fengyuanchen/datepicker
             *
             * Copyright (c) 2014-2017 Fengyuan Chen
             * Released under the MIT license
             *
             * Date: 2017-09-29T14:28:02.764Z
             */
            .datepicker-container{background-color:#fff;direction:ltr;font-size:12px;left:0;line-height:30px;position:fixed;top:0;-ms-touch-action:none;touch-action:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;width:210px;z-index:-1;-webkit-tap-highlight-color:transparent;-webkit-touch-callout:none}.datepicker-container:after,.datepicker-container:before{border:5px solid transparent;content:" ";display:block;height:0;position:absolute;width:0}.datepicker-dropdown{border:1px solid #ccc;box-shadow:0 3px 6px #ccc;box-sizing:content-box;position:absolute;z-index:1}.datepicker-inline{position:static}.datepicker-top-left,.datepicker-top-right{border-top-color:#39f}.datepicker-top-left:after,.datepicker-top-left:before,.datepicker-top-right:after,.datepicker-top-right:before{border-top:0;left:10px;top:-5px}.datepicker-top-left:before,.datepicker-top-right:before{border-bottom-color:#39f}.datepicker-top-left:after,.datepicker-top-right:after{border-bottom-color:#fff;top:-4px}.datepicker-bottom-left,.datepicker-bottom-right{border-bottom-color:#39f}.datepicker-bottom-left:after,.datepicker-bottom-left:before,.datepicker-bottom-right:after,.datepicker-bottom-right:before{border-bottom:0;bottom:-5px;left:10px}.datepicker-bottom-left:before,.datepicker-bottom-right:before{border-top-color:#39f}.datepicker-bottom-left:after,.datepicker-bottom-right:after{border-top-color:#fff;bottom:-4px}.datepicker-bottom-right:after,.datepicker-bottom-right:before,.datepicker-top-right:after,.datepicker-top-right:before{left:auto;right:10px}.datepicker-panel>ul{margin:0;padding:0;width:102%}.datepicker-panel>ul:after,.datepicker-panel>ul:before{content:" ";display:table}.datepicker-panel>ul:after{clear:both}.datepicker-panel>ul>li{background-color:#fff;cursor:pointer;float:left;height:30px;list-style:none;margin:0;padding:0;text-align:center;width:30px}.datepicker-panel>ul>li:hover{background-color:#e5f2ff}.datepicker-panel>ul>li.muted,.datepicker-panel>ul>li.muted:hover{color:#999}.datepicker-panel>ul>li.highlighted{background-color:#e5f2ff}.datepicker-panel>ul>li.highlighted:hover{background-color:#cce5ff}.datepicker-panel>ul>li.picked,.datepicker-panel>ul>li.picked:hover{color:#39f}.datepicker-panel>ul>li.disabled,.datepicker-panel>ul>li.disabled:hover{background-color:#fff;color:#ccc;cursor:default}.datepicker-panel>ul>li.disabled.highlighted,.datepicker-panel>ul>li.disabled:hover.highlighted{background-color:#e5f2ff}.datepicker-panel>ul>li[data-view="month next"],.datepicker-panel>ul>li[data-view="month prev"],.datepicker-panel>ul>li[data-view="year next"],.datepicker-panel>ul>li[data-view="year prev"],.datepicker-panel>ul>li[data-view="years next"],.datepicker-panel>ul>li[data-view="years prev"],.datepicker-panel>ul>li[data-view=next]{font-size:18px}.datepicker-panel>ul>li[data-view="month current"],.datepicker-panel>ul>li[data-view="year current"],.datepicker-panel>ul>li[data-view="years current"]{width:150px}.datepicker-panel>ul[data-view=months]>li,.datepicker-panel>ul[data-view=years]>li{height:52.5px;line-height:52.5px;width:52.5px}.datepicker-panel>ul[data-view=week]>li,.datepicker-panel>ul[data-view=week]>li:hover{background-color:#fff;cursor:default}.datepicker-hide{display:none}

        </style>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-date">Date</label>
            <input type="text" id="_polymnia-show-date" name="_polymnia-show-date" style="width: 80%" value="<?= wp_kses( $show_date, array( 'strong' => array(), 'a' => array('href') ) ) ?>" data-toggle="datepicker" required />
        </div>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-venue">Venue</label>
            <input type="text" id="_polymnia-show-venue" name="_polymnia-show-venue" style="width: 80%" value="<?= wp_kses( $show_venue, array( 'strong' => array(), 'a' => array('href') ) ) ?>" required />
        </div>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-city">City</label>
            <input type="text" id="_polymnia-show-city" name="_polymnia-show-city" style="width: 80%" value="<?= wp_kses( $show_city, array( 'strong' => array(), 'a' => array('href') ) )?>" />
        </div>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-showtime">Show Time</label>
            <input type="text" id="_polymnia-show-showtime" name="_polymnia-show-showtime" style="width: 80%" value="<?= wp_kses( $show_time, array( 'strong' => array(), 'a' => array('href') ) )?>" />
        </div>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-guests">Guests</label>
            <input type="text" id="_polymnia-show-guests" name="_polymnia-show-guests" style="width: 80%" value="<?= wp_kses( $show_guests, array( 'strong' => array(), 'a' => array('href') ) )?>" />
        </div>
        <div class="polymnia-show-field">
            <label for="_polymnia-show-tickets-link">Link to Buy Tickets</label>
            <input type="text" id="_polymnia-show-tickets-link" name="_polymnia-show-tickets-link" style="width: 80%" value="<?= esc_url_raw( $show_tickets_link )?>" />
        </div>
        <?php
    }
}


/**
 * Save Show Listing Title
 * @since 1.0.0
 * @param 	WP_Post 	$post    	Post object. Defaults to current post.
 * @param 	array 		$data 		An array of sanitized attachment post data.
 * @return 	array 		$data
 */
if( ! function_exists('polymnia_show_listing_save_title' ) ) {
	function polymnia_show_listing_save_title($data, $post) {
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $data;
		}

		if ( $post['post_type'] == 'polymnia_shows' && $_POST ) {
		    //Configure new post title
			$title = $_POST['_polymnia-show-date'] . ' - ' . $_POST['_polymnia-show-venue'];
			$data['post_title'] = $title;
            $data['post_name'] = sanitize_title( $title );

			return $data;
		}	else {
			return $data;
		}
	}

	add_filter('wp_insert_post_data', 'polymnia_show_listing_save_title', 99, 2);
}


/**
 * Save Show Schedule Listing
 * @since 1.0.0
 * @param 	int 	$post_id 	Post ID
 */
if ( ! function_exists( 'polymnia_show_schedule_save' ) ) :
	function polymnia_show_schedule_save( $post_id ) {
		if ( ! isset( $_POST['polymnia_show_date_nonce'] ) ||
		     ! isset( $_POST['polymnia_show_venue_nonce'] ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_date_nonce'], 'polymnia_show_schedule_nonce_action' ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_venue_nonce'], 'polymnia_show_schedule_nonce_action' ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_city_nonce'], 'polymnia_show_schedule_nonce_action' ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_showtime_nonce'], 'polymnia_show_schedule_nonce_action' ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_guests_nonce'], 'polymnia_show_schedule_nonce_action' ) ||
		     ! wp_verify_nonce( $_POST['polymnia_show_tickets_link_nonce'], 'polymnia_show_schedule_nonce_action' )
		) die('Nothing to see here...');

		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			return;

		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;

		if ( $_POST['_polymnia-show-date'] ) {
			$show_date = sanitize_text_field( $_POST['_polymnia-show-date'] );
			update_post_meta( $post_id, '_polymnia-show-date', $show_date );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-date' );
		}

		if ( $_POST['_polymnia-show-venue'] ) {
			$show_venue = sanitize_text_field( $_POST['_polymnia-show-venue'] );
			update_post_meta( $post_id, '_polymnia-show-venue', $show_venue );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-venue' );
		}

		if ( $_POST['_polymnia-show-city'] ) {
			$show_city = sanitize_text_field( $_POST['_polymnia-show-city'] );
			update_post_meta( $post_id, '_polymnia-show-city', $show_city );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-city' );
		}

		if ( $_POST['_polymnia-show-showtime'] ) {
			$show_showtime = sanitize_text_field( $_POST['_polymnia-show-showtime'] );
			update_post_meta( $post_id, '_polymnia-show-showtime', $show_showtime );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-showtime' );
		}

		if ( $_POST['_polymnia-show-guests'] ) {
			$show_guests = sanitize_text_field( $_POST['_polymnia-show-guests'] );
			update_post_meta( $post_id, '_polymnia-show-guests', $show_guests );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-guests');
		}

		if ( $_POST['_polymnia-show-tickets-link'] ) {
			$show_ticket_link = sanitize_text_field( $_POST['_polymnia-show-tickets-link'] );
			update_post_meta( $post_id, '_polymnia-show-tickets-link', $show_ticket_link );
		} else {
			delete_post_meta( $post_id, '_polymnia-show-tickets-link');
		}

	}
	add_action( 'save_post', 'polymnia_show_schedule_save' );
endif;

/**
 * Register API endpoints for show listings
 *
 * @since 1.0.0
 */
if( ! function_exists( 'polymnia_show_schedule_register_api' ) ) :
	function polymnia_show_schedule_register_api() {
	    //Register Date
		register_rest_field( 'polymnia_shows',
			'show_date',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);

		//Register Venue
		register_rest_field( 'polymnia_shows',
			'venue',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);

		//Register Venue
		register_rest_field( 'polymnia_shows',
			'city',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);

		//Register Show Time
		register_rest_field( 'polymnia_shows',
			'show_time',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);

		//Register Show Guests
		register_rest_field( 'polymnia_shows',
			'guests',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);

		//Register Show Tickets Link
		register_rest_field( 'polymnia_shows',
			'tickets_link',
			array(
				'get_callback'    => 'polymnia_show_schedule_get_cb',
				'update_callback' => 'polymnia_show_schedule_update_cb',
				'schema'          => null,
			)
		);
	}
    add_action( 'rest_api_init', 'polymnia_show_schedule_register_api' );
endif;

/**
 * Handler for getting show schedule data.
 *
 * @since 1.0.0
 *
 * @param array $object The object from the response
 * @param string $field_name Name of field
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function polymnia_show_schedule_get_cb( $object, $field_name ) {

    //translate the field name
    switch ( $field_name ) {
        case 'show_date':
            $field_name = '_polymnia-show-date';
            break;
        case 'venue':
            $field_name = '_polymnia-show-venue';
            break;
        case 'city':
            $field_name = '_polymnia-show-city';
            break;
        case 'show_time':
            $field_name = '_polymnia-show-showtime';
            break;
        case 'guests':
           $field_name = '_polymnia-show-guests';
           break;
        case 'tickets_link':
           $field_name = '_polymnia-show-tickets-link';
           break;
    }

    //return the post meta
    return get_post_meta( $object['id'], $field_name );

}


/**
 * Handler for updating show schedule data.
 *
 * @since 1.0.0
 *
 * @param mixed $value The value of the field
 * @param object $object The object from the response
 * @param string $field_name Name of field
 *
 * @return bool|int
 */
function polymnia_show_schedule_update_cb( $value, $object, $field_name ) {
	if ( ! $value || ! is_string( $value ) ) {
		return;
	}

	//translate the field name and sanitize data
	switch ( $field_name ) {
		case 'show_date':
			$field_name = '_polymnia-show-date';
			$value = sanitize_text_field( $value );
			break;
		case 'venue':
			$field_name = '_polymnia-show-venue';
			$value = sanitize_text_field( $value );
			break;
		case 'city':
			$field_name = '_polymnia-show-city';
			$value = sanitize_text_field( $value );
			break;
		case 'show_time':
			$field_name = '_polymnia-show-showtime';
			$value = sanitize_text_field( $value );
			break;
		case 'guests':
			$field_name = '_polymnia-show-guests';
			$value = sanitize_text_field( $value );
			break;
		case 'tickets_link':
			$field_name = '_polymnia-show-tickets-link';
			$value = esc_url_raw( $value );
			break;
	}

	return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );

}

/**
 * Add Show Schedule to RSS Feed
 *
 * @param $qv       query value
 *
 * @return array
 */
if( ! function_exists( 'polymnia_show_schedule_rss_request' ) ) {
	function polymnia_show_schedule_rss_request( $qv ) {
		if ( isset( $qv['feed'] ) && !isset( $qv['post_type'] ) ) {
			$qv['post_type'] = array( 'post', 'polymnia_shows' );
		}
		return $qv;
	}
	add_filter( 'request', 'polymnia_show_schedule_rss_request' );
}

/**
 * Add Show Schedule Meta to RSS Feed
 */
if( ! function_exists( 'polymnia_show_schedule_rss2_item' ) ) {
	function polymnia_show_schedule_rss2_item() {
		if ( get_post_type() == 'polymnia_shows' ) {
			$fields = array( '_polymnia-show-date', '_polymnia-show-venue', '_polymnia-show-city', '_polymnia-show-showtime', '_polymnia-show-guests', '_polymnia-show-tickets-link' );
			$post_id = get_the_ID();
			foreach( $fields as $field ) {
				if ( $value = get_post_meta( $post_id, $field, true ) ) {
					//Strip the _polymnia identifier from meta field
					$field = str_replace( '_polymnia-', '', $field );
					//Convert to camel case
					$field = lcfirst( str_replace( '-', '', ucwords( $field, '-' ) ) );

					echo "\t\t<{$field}>{$value}</{$field}>\n";

				}
			}
		}
	}
	add_action('rss2_item', 'polymnia_show_schedule_rss2_item');
}
