<?php
/**
 * POLYMNIA Theme Customizer
 *
 * @package POLYMNIA
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function polymnia_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'polymnia_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'polymnia_customize_partial_blogdescription',
		) );
	}

	//Change default background color
	$wp_customize->add_setting( 'background_color' , array(
		'default'   => '#030303',
		'transport' => 'refresh',
	) );

	//Change default header text color
	$wp_customize->add_setting( 'header_textcolor' , array(
		'default'   => '#FCFCFC',
		'transport' => 'refresh',
	) );

	//Add a setting for SVG site logo
	$wp_customize->add_setting('polymnia_logo_svg');
	// Add a control to upload the logo
	$wp_customize->add_control('your_theme_logo',
		array(
			'type'  => 'textarea',
			'label' => __( 'SVG Logo', 'polymnia' ),
			'section' => 'title_tagline',
			'settings' => 'polymnia_logo_svg',
		) );
}
add_action( 'customize_register', 'polymnia_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function polymnia_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function polymnia_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function polymnia_customize_preview_js() {
	wp_enqueue_script( 'polymnia-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'polymnia_customize_preview_js' );
