<?php
/**
 * Polymnia Admin Panel
 *
 * This file is read by WordPress to generate the plugin Polymnia admin panel.
 *
 * Text Domain:       polymnia
 */


// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



//Create Menu Item
if( ! function_exists('polymnia_admin_menu') ){
	function polymnia_admin_menu() {
		$page_title = __('Polymnia Options', 'polymnia');
		$menu_title = __('Polymnia', 'polymnia');
		$capability = 'manage_options';
		$menu_slug  = 'polymnia-admin';
		$function   = 'polymnia_admin_callback';
		$icon_url   = '/wp-content/themes/polymnia/images/polymnia-ico-menu.png';

		$submenu = array(
			'parent_slug' 	=> 'polymnia-admin',
			'page_title' 	=> 'Show Schedule',
			'menu_title'	=> 'Show Schedule',
			'capability'	=> 'manage_options',
			'menu_slug' 	=> 'edit.php?post_type=polymnia_shows',
			'function' 		=> 'polymnia_show_schedule_callback'
		);


		add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url );
		add_submenu_page($submenu['parent_slug'], $submenu['page_title'], $submenu['menu_title'], $submenu['capability'], $submenu['menu_slug'], NULL);
	}

	add_action( 'admin_menu', 'polymnia_admin_menu' );
}


function polymnia_admin_callback() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$my_theme = wp_get_theme();

	echo '<div class="wrap">';
	echo '<p>Thank you for choosing POLYMNIA!</p>';
	printf( '%1$s is version %2$s',
		$my_theme->get( 'Name' ),
		$my_theme->get( 'Version' )
	);
	echo '</div>';

	//Add hook for rendering data on POLYMNIA admin page
	do_action( 'polymnia_admin_page' );
}

function polymnia_show_schedule_callback() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}


	echo '<div class="wrap">';
	echo	'<h1 class="wp-heading-inline">Show List</h1>';

	echo	 '<a href="http://polymnia.dev/wp-admin/post-new.php?post_type=polymnia_shows" class="page-title-action">Add Show</a>';
	echo     '<hr class="wp-header-end">';


	echo '</div>';
}
